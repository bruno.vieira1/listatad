package entities;

import services.ContaBancariaService;

public class ContaBancaria implements ContaBancariaService {

	/*
	 * Sabendo que uma conta banc�ria tem as opera��es de visualizar saldo,
	 * depositar, sacar e atualizar. Crie uma TAD para representar essas opera��es.
	 */

	private double saldo;
	private int numeroConta;

	public ContaBancaria() {

	}

	public ContaBancaria( int numeroConta) {

		this.numeroConta = numeroConta;
	}

	public int getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}

	@Override
	public void depositar(double valor) {
		saldo+=valor;
		
	}

	@Override
	public void sacar(double valor) {
		saldo-=valor;
		
	}

	@Override
	public void atualizarContaBancaria(String nome, int numeroConta) {
		this.numeroConta=numeroConta;
		
	}

	@Override
	public double saldoContaBancaria() {
		return saldo;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("DADOS DA CONTA:"
				+ "\nNmero da conta: "+ numeroConta);
		sb.append("\nSaldo: "+ saldo);
		
		return sb.toString();
	}



}