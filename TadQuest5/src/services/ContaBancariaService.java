package services;

public interface ContaBancariaService  {
	
	
	public double saldoContaBancaria();
	public void depositar(double valor);
	public void sacar(double valor);
	public void atualizarContaBancaria(String nome, int numeroConta);
	
	
}
