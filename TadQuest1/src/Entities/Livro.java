package Entities;

import java.util.List;

import LivrosService.LivrosServices;

public class Livro implements LivrosServices {

	private String titulo;
	private String editora;
	private int anoPublicacao;

	public Livro() {

	}

	public Livro(String titulo, String editora, int anoPublicacao) {

		this.titulo = titulo;
		this.editora = editora;
		this.anoPublicacao = anoPublicacao;

	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public int getAnoPublicacao() {
		return anoPublicacao;
	}

	public void setAnoPublicacao(int anoPublicacao) {
		this.anoPublicacao = anoPublicacao;
	}

	@Override
	public void armazenarLivros(List<Livro> livros, Livro livro) {
		livros.add(livro);
	}

	@Override
	public String toString() {
		return "titulo= " + titulo + ", editora= " + editora + ", Publicacao= " + anoPublicacao + "";
	}
	

}
