package LivrosService;

import java.util.List;

import Entities.Livro;

public interface LivrosServices {

	/*
	 * Crie um TAD para armazenar dados de livros: T�tulo, editora, ano de
	 * publica��o.
	 */

	public void armazenarLivros(List<Livro> livros, Livro livro);

}
