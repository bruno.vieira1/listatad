package services;

import java.util.List;

import entities.Racionais;

public interface RacionaisService {

	public void armazenarRacionais (List <Racionais> racionais, Racionais racional);

}
