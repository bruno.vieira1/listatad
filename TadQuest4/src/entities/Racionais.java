package entities;

import java.util.List;

import services.RacionaisService;

public class Racionais implements RacionaisService {
	/*
	 * Crie um Tipo Abstrato de Dados (TAD) que represente os n�meros racionais e
	 * que contenha as seguintes funcionalidade de:
	 * 
	 * (a) Criar racional; (b) Somar racionais; (c) Multiplicar racionais; (d)
	 * testar igualdade.
	 */

	public Racionais() {

	}

	public double criar(double n1, double n2) {
		return n1 / n2;
	}

	public double multiplicar(double rac1, double rac2) {
		return rac1 * rac2;
	}

	public double soma(double soma1, double soma2) {
		return soma1 + soma2;
	}

	public boolean testar(double n1, double n2) {
		boolean res;

		if (n1 == n2) {
			res = true;
		} else {
			res = false;
		}
		return res;

	}

	@Override
	public void armazenarRacionais(List<Racionais> racionais, Racionais racional) {
		// TODO Auto-generated method stub
		
	}
}