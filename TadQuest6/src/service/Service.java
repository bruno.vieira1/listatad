package service;

import entities.Contato;

public interface Service {
	/*
	 * Sabendo que as opera��es de uma agenda telef�nica possuem as opera��es:
	 * Adicionar, excluir, atualizar, remover. Elabore uma TAD para representar
	 * essas opera��es.
	 */
	public void adicionar(Contato contato);
	public void remover(Contato contato);
	public void excluir();
	public void atualizar(Contato contato, String novoNome, int novoNumero);

}
