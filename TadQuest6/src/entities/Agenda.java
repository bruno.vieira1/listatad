package entities;

import service.Service;

public class Agenda implements Service {
	/*
	 * Sabendo que as opera��es de uma agenda telef�nica possuem as opera��es:
	 * Adicionar, excluir, atualizar, remover. Elabore uma TAD para representar
	 * essas opera��es.
	 */
	Contato[] lista = new Contato[20];

	@Override
	public void adicionar(Contato contato) {

		for (int i = 0; i < lista.length; i++) {

			if (lista[i] != null) {
				lista[0] = contato;
			} else {
				lista[i] = contato;
				i = lista.length;
			}
		}
	}

	@Override
	public void remover(Contato contato) {
		for (int i = 0; i < lista.length; i++) {

			if (lista[i] == contato) {
				lista[i] = null;
			}
		}
	}

	@Override
	public void excluir() {
		for (int i = 0; i < lista.length; i++) {
			lista[i] = null;
		}

	}

	@Override
	public void atualizar(Contato contato, String novoNome, int novoNumero) {

		for (int i = 0; i < lista.length; i++) {
			if (lista[i] == contato) {
				lista[i].nome = novoNome;
				lista[i].numero = novoNumero;

			}
		}

	}

}
