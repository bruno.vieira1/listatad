package services;

import java.util.List;

import entities.Alunos;

public interface Objeto {
	
	public void armazenarDados(List<Alunos> alunos, Alunos aluno);
}
