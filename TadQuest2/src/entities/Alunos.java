package entities;

import java.util.List;

public class Alunos {

	private String nome;
	private int semestre;

	public Alunos() {

	}

	public Alunos(String nome, int semestre) {
		this.nome = nome;
		this.semestre = semestre;
	}
	

	@Override
	public String toString() {
		return "Alunos [nome=" + nome + ", semestre=" + semestre + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getSemestre() {
		return semestre;
	}

	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}

	public void armazenarDados(List<Alunos> alunos, Alunos aluno) {
		alunos.add(aluno);
	}

}
