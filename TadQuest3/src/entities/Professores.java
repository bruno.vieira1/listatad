package entities;

import java.util.List;

public class Professores {

	private String nome;
	private int disciplinaCod;
	
	
	
	public Professores() {
		
	}
	
	public Professores(String nome, int disciplinaCod) {
		this.nome=nome;
		this.disciplinaCod=disciplinaCod;
	}
	

	@Override
	public String toString() {
		return "Professores [nome=" + nome + ", disciplinaCod=" + disciplinaCod + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getDisciplinaCod() {
		return disciplinaCod;
	}

	public void setDisciplinaCod(int disciplinaCod) {
		this.disciplinaCod = disciplinaCod;
	}
public void armazenarDados(List<Professores> professores, Professores professor) {
		professores.add(professor);
	}
	
	
	
	
}
